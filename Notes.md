# Developer Notes

We use [poetry](https://python-poetry.org/) to manage dependencies in
[`pyproject.toml`](pyproject.toml).  This should be run to generate the
`requirements.txt` file needed by [mybinder.org](https://mybinder.org) before the final
commit:

```bash
poetry export -f requirements.txt -o requirements.txt
```

## Binder

Some binary libraries are required for [PyVista] on [Binder] or one will obtain errors
like:

```
ImportError: libGL.so.1: cannot open shared object file: No such file or directory
```

These binary dependencies are specified in `apt.txt`.

PyVista requires some binary libraries that are not available on Binder if we use
`requirements.txt`, so we use `conda` instead by specifying an `environment.yml` file.
This cannot be managed by [poetry] yet.

* https://github.com/pyvista/cookiecutter-pyvista-binder/

[poetry]: <https://python-poetry.org/> "Poetry"
