"""Classical trajectories.

We use complex coordinates `q = x + 1j*z` where the acceleration points in the -z
direction.  We take `z=0` as the injection site, and locate the scattering potentials
through their positions `q`, strength `U`, and waist `sigma`.
"""
from collections import namedtuple

import numpy as np

from scipy.integrate import solve_ivp

__all__ = ["Trajectories", "Experiment", "u"]

Trajectories = namedtuple("Trajectories", ["t", "q", "v"])


class Units(object):
    micron = 1.0
    m = 1e6 * micron
    ms = 1.0
    s = 1000 * ms
    mRb = 1.0  # Rb 87 mass

    # 1 microK * kB / mRb
    microK_mRb = 95.668397185979197345 * micron ** 2 / ms ** 2

    # Acceleration from paper
    a_z = 26.9 * m / s ** 2


u = Units()


class GaussianV(object):
    """Represents a Gaussian potential.

    Attributes
    ----------
    q : complex
       Location of center `q = x + 1j*z`.
    sigma : float
       Optical waist.
    U_m : float
       Strength.
    epsilon, a_z : float, optional
       Optional way of specifying U_m = epsilon * abs(a_z*q.imag).
    """

    def __init__(self, q, sigma, U_m=None, epsilon=None, a_z=u.a_z):
        self.a_z = a_z
        self.q = q
        if U_m is None:
            U_m = epsilon * abs(self.a_z * self.q.imag)
        self.sigma = sigma
        self.U_m = U_m

    def V_m(self, q):
        """Return `V_m(q)`."""
        return self.U_m * np.exp(-2 * (abs(q - self.q) / self.sigma) ** 2)

    def a(self, q):
        """Return the acceleration `a(q) = F(q)/m = -grad(V)(q)/m`."""
        V_m = self.V_m(q)
        return 4 * (q - self.q) * V_m / self.sigma ** 2

    @property
    def epsilon(self):
        return self.U_m / abs(self.q.imag * self.a_z)

    def __repr__(self):
        return (
            f"{self.__class__.__name__}"
            + f"(epsilon={self.epsilon:4.6g}, q={self.q}, sigma={self.sigma})"
        )


class Experiment(object):
    """Class defining experimental parameters such as the location of the potentials.

    Attributes
    ----------
    Vs : [(q, U/m, sigma)]
       List of potential parameters `q` (position), `U` (strength), and `sigma`
       (waist).  These potentials each have the form::

           V(x + 1j*z) = U*exp(-2*(abs(x + 1j*zq - q)/sigma)**2)/m
    a : complex
       Acceleration.
    x0_max : float
       Maximum impact parameter.
    Nx0 : float
       Number of trajectories.
    z_min : float
       Minimum z value.  We terminate the integration once the particles fall this far.
    t_tf : float
       Maximum time to simulate in units of the free-fall time tf for particles to fall
       from the injection site to z_min.
    """

    # Potential parameters from paper.
    paper_Vs = {
        "fig1": [
            GaussianV(
                q=(0 - 85j) * u.micron, U_m=23.1 * u.microK_mRb, sigma=10.3 * u.micron
            ),
            GaussianV(
                q=(39 - 85j - 42j) * u.micron,
                U_m=23.1 * u.microK_mRb,
                sigma=10.3 * u.micron,
            ),
        ],
        "fig3a": [GaussianV(epsilon=0.3, q=-78j * u.micron, sigma=11.3 * u.micron)],
        "fig3b": [GaussianV(epsilon=0.9, q=-78j * u.micron, sigma=11.3 * u.micron)],
        "fig3c": [GaussianV(epsilon=0.99, q=-78j * u.micron, sigma=11.3 * u.micron)],
        "fig3d": [GaussianV(epsilon=1.4, q=-78j * u.micron, sigma=11.3 * u.micron)],
        "fig3e": [GaussianV(epsilon=-0.1, q=-46j * u.micron, sigma=27 * u.micron)],
        "fig3f": [GaussianV(epsilon=-0.3, q=-46j * u.micron, sigma=27 * u.micron)],
        "fig3g": [GaussianV(epsilon=-0.9, q=-46j * u.micron, sigma=27 * u.micron)],
        "fig3h": [GaussianV(epsilon=-1.8, q=-46j * u.micron, sigma=27 * u.micron)],
    }

    paper_Vs["fig4a"] = paper_Vs["fig4e"] = paper_Vs["fig3b"]
    paper_Vs["fig4b"] = paper_Vs["fig4f"] = paper_Vs["fig3d"]
    paper_Vs["fig4c"] = paper_Vs["fig4g"] = paper_Vs["fig3f"]
    paper_Vs["fig4d"] = paper_Vs["fig4h"] = paper_Vs["fig3h"]

    def __init__(
        self,
        Vs="fig1",
        a=-1j * u.a_z,
        z_min=-400 * u.micron,
        x0_max=80 * u.micron,
        Nx0=100,
        t_tf=5.0,
    ):
        if isinstance(Vs, str):
            self.Vs = self.paper_Vs[Vs]
        else:
            self.Vs = Vs
        self.a = a
        self.x0_max = x0_max
        self.z_min = z_min
        self.t_tf = t_tf

        self._t0 = 1.0

        # Free-fall time for particles to get to z_min.
        self._tf = np.sqrt(abs(self.z_min / self.a))

        self._x0s = np.linspace(-self.x0_max, self.x0_max, Nx0) * u.micron
        self._qv0 = (self._x0s + 0j, 0j * self._x0s)

    def compute_dy_dt(self, t, y):
        """Return `dy_dt` for `solve_ivp`.

        Arguments
        ---------
        t : float
           Time.
        y : array-like
           Flat array of `[q, dq_dt]`.
        """
        # y = (q, dq_dt)
        q, dq_dt = y.reshape((2, len(y) // 2))
        ddq_dtt = self.a + sum(_V.a(q) for _V in self.Vs)

        return np.array([dq_dt, ddq_dtt]).ravel()

    def _event(self, t, y):
        """Termination event: when all particles fall out of frame."""
        z, v = y.reshape((2, len(y) // 2))
        return z.imag.max() - self.z_min

    _event.terminal = True

    def solve(
        self,
        t=None,
        t_eval=None,
        qv0=None,
        res=None,
        atol=1e-8,
        rtol=1e-10,
        method="DOP853",  # "RK45",
        max_step_factor=200,
        **kw,
    ):
        """Return `Trajectories` object that solves the classical problem.

        Arguments
        ---------
        max_step_factor : float
           How many substeps to divide `self._t0` into to solve problem.

        Returns
        -------
        sol : Trajectories
           Classical trajectories as a `Trajectories` object which contains `sol.t`,
           `sol.q`, `sol.v`, the first index of which corresponds to time.
        """

        # atol=1e-8, rtol=1e-10, method='BDF', **kw):
        if t is None:
            t = self.t_tf * self._tf

        if qv0 is None:
            qv0 = self._qv0

        y0 = np.ravel(qv0)
        N = len(y0) // 2

        sol = solve_ivp(
            self.compute_dy_dt,
            t_span=(0, t),
            y0=y0,
            events=[self._event],
            t_eval=t_eval,
            atol=atol,
            rtol=rtol,
            max_step=self._t0 / max_step_factor,
            method=method,
            **kw,
        )
        t = sol.t
        sol.q, sol.v = sol.y.reshape((2, N, len(t)))
        assert sol.status in set([0, 1])  # Termination event or t

        sol = Trajectories(t=sol.t, q=sol.q.T, v=sol.v.T)

        return sol

    def get_dense_x0s(self, x0s, dqf=None, **kw):
        """Returns `(sol, x0s)` where `x0s` gives a dense set of trajectories.

        See `get_dense_sol()` for arguments.

        Returns
        -------
        x0s : array
           New set of `x0s` for which the solutions are dense.
        sol : Sol
           Result of solve on the initial set of `x0s`.
        """
        x0s = np.asarray(x0s)
        if dqf is None:
            dqf = abs(np.diff(x0s)).min()

        q0s = 0j + x0s
        sol0 = self.solve(qv0=[q0s, 0 * q0s], **kw)
        qfs = sol0.q[-1, :]
        Ns = np.ceil(abs(np.diff(qfs)) / dqf).astype(int)
        x1s = []
        for (_x0, _x1, _N) in zip(x0s[:-1], x0s[1:], Ns):
            if _N > 1:
                x1s.extend(np.linspace(_x0, _x1, _N + 1)[1:-1])

        x1s = np.union1d(x0s, x1s)
        return x1s, sol0

    def get_dense_sol(self, x0s=None, dqf=None, **kw):
        """Return `(sol0, sol1)`, a sparse and dense set of solutions starting with `x0s`.

        The solver is first run with `x0s`, then an estimate is made about how to
        subdivide the intervals, and the solutions are recomputed using these
        subdivisions.

        Arguments
        ---------
        x0s : array-like, None
           Initial positions.
        dqf : float, None
           Target separation between final positions.  If None, then use
           `dqf = min(abs(diff(x0)))`.

        Returns
        -------
        sol0 : Sol
           Solution starting with `x0s`
        sol1 : Sol
           Roughly density solution.
        """
        if x0s is None:
            x0s = self._x0s
        x1s, sol0 = self.get_dense_x0s(x0s=x0s, dqf=dqf, **kw)
        q1s = 0j + x1s
        sol1 = self.solve(qv0=[q1s, 0 * q1s], **kw)
        return sol0, sol1
