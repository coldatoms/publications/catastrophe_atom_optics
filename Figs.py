# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.11.1
#   kernelspec:
#     display_name: Python [conda env:_catastrophe_atom_optics]
#     language: python
#     name: conda-env-_catastrophe_atom_optics-py
# ---

# # General VTKjs Files

# Here we run through all figures and generate VTKjs files that can be used to view these models offline by running them through [viewer.pyvista.org](https://viewer.pyvista.org) or [kitware.github.io](https://kitware.github.io/vtk-js/examples/SceneExplorer/index.html):
#
# * Two repulsive potentials: [Fig.1](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig1.vtkjs)
# * Single repulsive Gaussian potential: [Fig.3a](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3a.vtkjs), [Fig.3b](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3b.vtkjs), [Fig.3c](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3c.vtkjs), [Fig.3d](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3d.vtkjs)
# * Single attractive Gaussian potential: [Fig.3e](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3e.vtkjs), [Fig.3f](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3f.vtkjs), [Fig.3g](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3g.vtkjs), [Fig.3h](https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/fig3h.vtkjs).

from IPython.display import HTML, IFrame
keys = ["Fig.1", "Fig.3a", "Fig.3b", "Fig.3c", "Fig.3d", "Fig.3e", "Fig.3f", "Fig.3g", "Fig.3h"]
URL = "https://viewer.pyvista.org/?fileURL=https://swan.physics.wsu.edu/Public/papers/Mossman_2021/"
files = {key: key.replace(".", "").lower() + ".vtkjs" for key in keys}
frames = {key: IFrame(width="auto", height="auto", src=f"{URL}/{files[key]}") for key in keys}
figs = [f'<div width="25%" style="display: inline-block;"><p>{key}</p>{frames[key]._repr_html_()}</div>' 
        for key in keys]
fig1 = figs[0]
fig3r = "".join(figs[1:5])
fig3a = "".join(figs[5:])
HTML('</br>'.join([
    f'{fig1}',
    f'<div style="display: block;">{fig3r}</div>',
    f'<div style="display: block;">{fig3a}</div>']))
